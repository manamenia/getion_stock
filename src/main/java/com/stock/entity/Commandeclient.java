/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Hamza
 */
@Entity
@Table(name = "commandeclient")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commandeclient.findAll", query = "SELECT c FROM Commandeclient c"),
    @NamedQuery(name = "Commandeclient.findByIdCmdClient", query = "SELECT c FROM Commandeclient c WHERE c.idCmdClient = :idCmdClient"),
    @NamedQuery(name = "Commandeclient.findByCode", query = "SELECT c FROM Commandeclient c WHERE c.code = :code"),
    @NamedQuery(name = "Commandeclient.findByDateCmde", query = "SELECT c FROM Commandeclient c WHERE c.dateCmde = :dateCmde")})
public class Commandeclient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idCmdClient")
    private Integer idCmdClient;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dateCmde")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCmde;
    
    @ManyToOne
    @JoinColumn(name = "idClient", referencedColumnName = "idClient")
    private Client idClient;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCmdClient")
    private Collection<Lignecmdclient> lignecmdclientCollection;

    public Commandeclient() {
    }

    public Commandeclient(Integer idCmdClient) {
        this.idCmdClient = idCmdClient;
    }

    public Commandeclient(Integer idCmdClient, String code, Date dateCmde) {
        this.idCmdClient = idCmdClient;
        this.code = code;
        this.dateCmde = dateCmde;
    }

    public Integer getIdCmdClient() {
        return idCmdClient;
    }

    public void setIdCmdClient(Integer idCmdClient) {
        this.idCmdClient = idCmdClient;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDateCmde() {
        return dateCmde;
    }

    public void setDateCmde(Date dateCmde) {
        this.dateCmde = dateCmde;
    }

    public Client getIdClient() {
        return idClient;
    }

    public void setIdClient(Client idClient) {
        this.idClient = idClient;
    }

    @XmlTransient
    public Collection<Lignecmdclient> getLignecmdclientCollection() {
        return lignecmdclientCollection;
    }

    public void setLignecmdclientCollection(Collection<Lignecmdclient> lignecmdclientCollection) {
        this.lignecmdclientCollection = lignecmdclientCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCmdClient != null ? idCmdClient.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commandeclient)) {
            return false;
        }
        Commandeclient other = (Commandeclient) object;
        if ((this.idCmdClient == null && other.idCmdClient != null) || (this.idCmdClient != null && !this.idCmdClient.equals(other.idCmdClient))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.stock.entity.Commandeclient[ idCmdClient=" + idCmdClient + " ]";
    }
    
}
