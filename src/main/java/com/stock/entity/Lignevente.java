/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Hamza
 */
@Entity
@Table(name = "lignevente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lignevente.findAll", query = "SELECT l FROM Lignevente l"),
    @NamedQuery(name = "Lignevente.findByIdLigneVente", query = "SELECT l FROM Lignevente l WHERE l.idLigneVente = :idLigneVente")})
public class Lignevente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idLigneVente")
    private Integer idLigneVente;
    @JoinColumn(name = "vente", referencedColumnName = "idVente")
    @ManyToOne(optional = false)
    private Vente vente;
    @JoinColumn(name = "idArticle", referencedColumnName = "idArticle")
    @ManyToOne(optional = false)
    private Article idArticle;

    public Lignevente() {
    }

    public Lignevente(Integer idLigneVente) {
        this.idLigneVente = idLigneVente;
    }

    public Integer getIdLigneVente() {
        return idLigneVente;
    }

    public void setIdLigneVente(Integer idLigneVente) {
        this.idLigneVente = idLigneVente;
    }

    public Vente getVente() {
        return vente;
    }

    public void setVente(Vente vente) {
        this.vente = vente;
    }

    public Article getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(Article idArticle) {
        this.idArticle = idArticle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLigneVente != null ? idLigneVente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lignevente)) {
            return false;
        }
        Lignevente other = (Lignevente) object;
        if ((this.idLigneVente == null && other.idLigneVente != null) || (this.idLigneVente != null && !this.idLigneVente.equals(other.idLigneVente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.stock.entity.Lignevente[ idLigneVente=" + idLigneVente + " ]";
    }
    
}
