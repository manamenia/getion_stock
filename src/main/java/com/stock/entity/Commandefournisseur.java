/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Hamza
 */
@Entity
@Table(name = "commandefournisseur")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commandefournisseur.findAll", query = "SELECT c FROM Commandefournisseur c"),
    @NamedQuery(name = "Commandefournisseur.findByIdCmdFournis", query = "SELECT c FROM Commandefournisseur c WHERE c.idCmdFournis = :idCmdFournis"),
    @NamedQuery(name = "Commandefournisseur.findByCode", query = "SELECT c FROM Commandefournisseur c WHERE c.code = :code"),
    @NamedQuery(name = "Commandefournisseur.findByDateCmde", query = "SELECT c FROM Commandefournisseur c WHERE c.dateCmde = :dateCmde")})
public class Commandefournisseur implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idCmdFournis")
    private Integer idCmdFournis;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "code")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dateCmde")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCmde;

    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCmdFournis")
    private Collection<Lignecmdclient> lignecmdfournisCollection;
    
    @ManyToOne
    @JoinColumn(name = "idFournisseur", referencedColumnName = "idFournisseur")
    private Fournisseur idFournisseur;

    public Commandefournisseur() {
    }

    public Commandefournisseur(Integer idCmdFournis) {
        this.idCmdFournis = idCmdFournis;
    }

    public Commandefournisseur(Integer idCmdFournis, String code, Date dateCmde) {
        this.idCmdFournis = idCmdFournis;
        this.code = code;
        this.dateCmde = dateCmde;
    }
    
    
    public Collection<Lignecmdclient> getLignecmdfournisCollection() {
        return lignecmdfournisCollection;
    }

    public void setLignecmdfournisCollection(Collection<Lignecmdclient> lignecmdfournisCollection) {
        this.lignecmdfournisCollection = lignecmdfournisCollection;
    }

    public Integer getIdCmdFournis() {
        return idCmdFournis;
    }

    public void setIdCmdFournis(Integer idCmdFournis) {
        this.idCmdFournis = idCmdFournis;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getDateCmde() {
        return dateCmde;
    }

    public void setDateCmde(Date dateCmde) {
        this.dateCmde = dateCmde;
    }

    

    public Fournisseur getIdFournisseur() {
        return idFournisseur;
    }

    public void setIdFournisseur(Fournisseur idFournisseur) {
        this.idFournisseur = idFournisseur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCmdFournis != null ? idCmdFournis.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commandefournisseur)) {
            return false;
        }
        Commandefournisseur other = (Commandefournisseur) object;
        if ((this.idCmdFournis == null && other.idCmdFournis != null) || (this.idCmdFournis != null && !this.idCmdFournis.equals(other.idCmdFournis))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.stock.entity.Commandefournisseur[ idCmdFournis=" + idCmdFournis + " ]";
    }
    
}
