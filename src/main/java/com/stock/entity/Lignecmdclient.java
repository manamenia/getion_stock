/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Hamza
 */
@Entity
@Table(name = "lignecmdclient")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lignecmdclient.findAll", query = "SELECT l FROM Lignecmdclient l"),
    @NamedQuery(name = "Lignecmdclient.findByIdLigneCmdClient", query = "SELECT l FROM Lignecmdclient l WHERE l.idLigneCmdClient = :idLigneCmdClient")})
public class Lignecmdclient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idLigneCmdClient")
    private Integer idLigneCmdClient;
    @JoinColumn(name = "idCmdClient", referencedColumnName = "idCmdClient")
    @ManyToOne(optional = false)
    private Commandeclient idCmdClient;
    @JoinColumn(name = "idArticle", referencedColumnName = "idArticle")
    @ManyToOne(optional = false)
    private Article idArticle;

    public Lignecmdclient() {
    }

    public Lignecmdclient(Integer idLigneCmdClient) {
        this.idLigneCmdClient = idLigneCmdClient;
    }

    public Integer getIdLigneCmdClient() {
        return idLigneCmdClient;
    }

    public void setIdLigneCmdClient(Integer idLigneCmdClient) {
        this.idLigneCmdClient = idLigneCmdClient;
    }

    public Commandeclient getIdCmdClient() {
        return idCmdClient;
    }

    public void setIdCmdClient(Commandeclient idCmdClient) {
        this.idCmdClient = idCmdClient;
    }

    public Article getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(Article idArticle) {
        this.idArticle = idArticle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLigneCmdClient != null ? idLigneCmdClient.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lignecmdclient)) {
            return false;
        }
        Lignecmdclient other = (Lignecmdclient) object;
        if ((this.idLigneCmdClient == null && other.idLigneCmdClient != null) || (this.idLigneCmdClient != null && !this.idLigneCmdClient.equals(other.idLigneCmdClient))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.stock.entity.Lignecmdclient[ idLigneCmdClient=" + idLigneCmdClient + " ]";
    }
    
}
