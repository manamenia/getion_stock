/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Hamza
 */
@Entity
@Table(name = "lignecmdfournis")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lignecmdfournis.findAll", query = "SELECT l FROM Lignecmdfournis l"),
    @NamedQuery(name = "Lignecmdfournis.findByIdLigneCmdFournis", query = "SELECT l FROM Lignecmdfournis l WHERE l.idLigneCmdFournis = :idLigneCmdFournis")})
public class Lignecmdfournis implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idLigneCmdFournis")
    private Integer idLigneCmdFournis;
    @JoinColumn(name = "idCmdFournis", referencedColumnName = "idCmdFournis")
    @OneToOne(optional = false)
    private Commandefournisseur idCmdFournis;
    @JoinColumn(name = "idArticle", referencedColumnName = "idArticle")
    @OneToOne(optional = false)
    private Article idArticle;

    public Lignecmdfournis() {
    }

    public Lignecmdfournis(Integer idLigneCmdFournis) {
        this.idLigneCmdFournis = idLigneCmdFournis;
    }

    public Integer getIdLigneCmdFournis() {
        return idLigneCmdFournis;
    }

    public void setIdLigneCmdFournis(Integer idLigneCmdFournis) {
        this.idLigneCmdFournis = idLigneCmdFournis;
    }

    public Commandefournisseur getIdCmdFournis() {
        return idCmdFournis;
    }

    public void setIdCmdFournis(Commandefournisseur idCmdFournis) {
        this.idCmdFournis = idCmdFournis;
    }

    public Article getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(Article idArticle) {
        this.idArticle = idArticle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLigneCmdFournis != null ? idLigneCmdFournis.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lignecmdfournis)) {
            return false;
        }
        Lignecmdfournis other = (Lignecmdfournis) object;
        if ((this.idLigneCmdFournis == null && other.idLigneCmdFournis != null) || (this.idLigneCmdFournis != null && !this.idLigneCmdFournis.equals(other.idLigneCmdFournis))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.stock.entity.Lignecmdfournis[ idLigneCmdFournis=" + idLigneCmdFournis + " ]";
    }
    
}
