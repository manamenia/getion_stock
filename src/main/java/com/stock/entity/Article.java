/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Hamza
 */
@Entity
@Table(name = "article")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Article.findAll", query = "SELECT a FROM Article a"),
    @NamedQuery(name = "Article.findByIdArticle", query = "SELECT a FROM Article a WHERE a.idArticle = :idArticle"),
    @NamedQuery(name = "Article.findByCodeArticle", query = "SELECT a FROM Article a WHERE a.codeArticle = :codeArticle"),
    @NamedQuery(name = "Article.findByDesignation", query = "SELECT a FROM Article a WHERE a.designation = :designation"),
    @NamedQuery(name = "Article.findByPrixUnitaire", query = "SELECT a FROM Article a WHERE a.prixUnitaire = :prixUnitaire"),
    @NamedQuery(name = "Article.findByTauxTVA", query = "SELECT a FROM Article a WHERE a.tauxTVA = :tauxTVA"),
    @NamedQuery(name = "Article.findByPrixUTTC", query = "SELECT a FROM Article a WHERE a.prixUTTC = :prixUTTC"),
    @NamedQuery(name = "Article.findByPhoto", query = "SELECT a FROM Article a WHERE a.photo = :photo")})
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idArticle")
    private Integer idArticle;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "codeArticle")
    private String codeArticle;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "designation")
    private String designation;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prixUnitaire")
    private int prixUnitaire;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tauxTVA")
    private int tauxTVA;
    @Basic(optional = false)
    @NotNull
    @Column(name = "prixUTTC")
    private int prixUTTC;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "photo")
    private String photo;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "idArticle")
    private Lignecmdfournis lignecmdfournis;  
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idArticle")
    private Collection<Lignevente> ligneventeCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idArticle")
    private Collection<Mvtstock> mvtstockCollection;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idArticle")
    private Collection<Lignecmdclient> lignecmdclientCollection;
    
    @ManyToOne
    @JoinColumn(name = "idCategorie", referencedColumnName = "idCategory")
    private Category idCategorie;

    public Article() {
    }

    public Article(Integer idArticle) {
        this.idArticle = idArticle;
    }

    public Article(Integer idArticle, String codeArticle, String designation, int prixUnitaire, int tauxTVA, int prixUTTC, String photo) {
        this.idArticle = idArticle;
        this.codeArticle = codeArticle;
        this.designation = designation;
        this.prixUnitaire = prixUnitaire;
        this.tauxTVA = tauxTVA;
        this.prixUTTC = prixUTTC;
        this.photo = photo;
    }

    public Integer getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(Integer idArticle) {
        this.idArticle = idArticle;
    }

    public String getCodeArticle() {
        return codeArticle;
    }

    public void setCodeArticle(String codeArticle) {
        this.codeArticle = codeArticle;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public int getPrixUnitaire() {
        return prixUnitaire;
    }

    public void setPrixUnitaire(int prixUnitaire) {
        this.prixUnitaire = prixUnitaire;
    }

    public int getTauxTVA() {
        return tauxTVA;
    }

    public void setTauxTVA(int tauxTVA) {
        this.tauxTVA = tauxTVA;
    }

    public int getPrixUTTC() {
        return prixUTTC;
    }

    public void setPrixUTTC(int prixUTTC) {
        this.prixUTTC = prixUTTC;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Lignecmdfournis getLignecmdfournis() {
        return lignecmdfournis;
    }

    public void setLignecmdfournis(Lignecmdfournis lignecmdfournis) {
        this.lignecmdfournis = lignecmdfournis;
    }

    @XmlTransient
    public Collection<Lignevente> getLigneventeCollection() {
        return ligneventeCollection;
    }

    public void setLigneventeCollection(Collection<Lignevente> ligneventeCollection) {
        this.ligneventeCollection = ligneventeCollection;
    }

    @XmlTransient
    public Collection<Mvtstock> getMvtstockCollection() {
        return mvtstockCollection;
    }

    public void setMvtstockCollection(Collection<Mvtstock> mvtstockCollection) {
        this.mvtstockCollection = mvtstockCollection;
    }

    @XmlTransient
    public Collection<Lignecmdclient> getLignecmdclientCollection() {
        return lignecmdclientCollection;
    }

    public void setLignecmdclientCollection(Collection<Lignecmdclient> lignecmdclientCollection) {
        this.lignecmdclientCollection = lignecmdclientCollection;
    }

    public Category getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(Category idCategorie) {
        this.idCategorie = idCategorie;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idArticle != null ? idArticle.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Article)) {
            return false;
        }
        Article other = (Article) object;
        if ((this.idArticle == null && other.idArticle != null) || (this.idArticle != null && !this.idArticle.equals(other.idArticle))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.stock.entity.Article[ idArticle=" + idArticle + " ]";
    }
    
}
