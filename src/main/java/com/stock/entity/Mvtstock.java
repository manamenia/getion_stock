/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Hamza
 */
@Entity
@Table(name = "mvtstock")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mvtstock.findAll", query = "SELECT m FROM Mvtstock m"),
    @NamedQuery(name = "Mvtstock.findByIdMvt", query = "SELECT m FROM Mvtstock m WHERE m.idMvt = :idMvt"),
    @NamedQuery(name = "Mvtstock.findByDate", query = "SELECT m FROM Mvtstock m WHERE m.date = :date"),
    @NamedQuery(name = "Mvtstock.findByQuantite", query = "SELECT m FROM Mvtstock m WHERE m.quantite = :quantite"),
    @NamedQuery(name = "Mvtstock.findByTypeMvt", query = "SELECT m FROM Mvtstock m WHERE m.typeMvt = :typeMvt")})
public class Mvtstock implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idMvt")
    private Integer idMvt;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantite")
    private int quantite;
    @Basic(optional = false)
    @NotNull
    @Column(name = "typeMvt")
    private int typeMvt;
    @JoinColumn(name = "idArticle", referencedColumnName = "idArticle")
    @ManyToOne(optional = false)
    private Article idArticle;

    public Mvtstock() {
    }

    public Mvtstock(Integer idMvt) {
        this.idMvt = idMvt;
    }

    public Mvtstock(Integer idMvt, Date date, int quantite, int typeMvt) {
        this.idMvt = idMvt;
        this.date = date;
        this.quantite = quantite;
        this.typeMvt = typeMvt;
    }

    public Integer getIdMvt() {
        return idMvt;
    }

    public void setIdMvt(Integer idMvt) {
        this.idMvt = idMvt;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public int getTypeMvt() {
        return typeMvt;
    }

    public void setTypeMvt(int typeMvt) {
        this.typeMvt = typeMvt;
    }

    public Article getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(Article idArticle) {
        this.idArticle = idArticle;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMvt != null ? idMvt.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mvtstock)) {
            return false;
        }
        Mvtstock other = (Mvtstock) object;
        if ((this.idMvt == null && other.idMvt != null) || (this.idMvt != null && !this.idMvt.equals(other.idMvt))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.stock.entity.Mvtstock[ idMvt=" + idMvt + " ]";
    }
    
}
