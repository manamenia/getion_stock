/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.dao.Impl;

import com.stock.dao.IGenericDao;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.SessionFactory;

/**
 *
 * @author Hamza
 */
public class GenericDaoImpl<E> implements IGenericDao<E> {
    
    
    private SessionFactory sessionFactory;


    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    private Class<E> type;

    public GenericDaoImpl() {  
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type= (Class<E>) pt.getActualTypeArguments()[0];     
    }

    @Override
    public E save(E entity) {
       sessionFactory.getCurrentSession().persist(entity);
        return entity;
    }

    @Override
    public E update(E entity) {
        sessionFactory.getCurrentSession().merge(entity);
        return entity;               
    }

    @Override
    public List<E> selectAll() {
          Query query =sessionFactory.getCurrentSession().createQuery("select t from "+ type.getSimpleName()+" t");     
          return query.list();
    }

    @Override
    public List<E> selectAll(String sortField, String sort) {
          Query query =sessionFactory.getCurrentSession().createQuery("select t from "+ type.getSimpleName()+" t order by "+sortField+ " " +sort);     
          return query.list();    
    }

    @Override
    public E getById(int id) {
        return (E) sessionFactory.getCurrentSession().get(type.getSimpleName(), id);
    }

    @Override
    public void remove(int id) {        
        E ele = getById(id);
        sessionFactory.getCurrentSession().delete(ele);
    }

    @Override
    public E findOne(String paramNum, Object paramValue) {

        Query query =sessionFactory.getCurrentSession().createQuery("select t from "+ type.getSimpleName()+" t where "+paramNum+ " = :x");  
        query.setParameter(paramNum, paramValue);
        return( query.list().size() > 0 ? (E) query.list().get(0) : null);
    }

    @Override
    public E findOne(String[] paramNum, Object[] paramValue) {

        if(paramNum.length != paramValue.length){
            return null;
        }
        int len = paramValue.length;
        String queryString =("select t from "+ type.getSimpleName()+" t where ");  
        for(int i=0;i<len;i++){
            queryString +=" t "+ paramNum[i]+" = :x "+i;
            if((i+1)< len){
                queryString+= " and ";
            }
        }
         Query query =sessionFactory.getCurrentSession().createQuery(queryString);
         for(int i=0;i<paramValue.length ;i++){
             query.setParameter(paramNum[i], paramValue[i]);   
        }
        return ( query.list().size() > 0 ? (E) query.list().get(0) : null);
    }

    @Override
    public int findCountBy(String paramNum, String paramValue) {
        
        Query query =sessionFactory.getCurrentSession().createQuery("select t from "+ type.getSimpleName()+" t where "+paramNum+ " = :x");  
        query.setParameter(paramNum, paramValue);
        return( query.list().size() > 0 ? ((int) query.list().get(0)) : 0);
    }
    
}
