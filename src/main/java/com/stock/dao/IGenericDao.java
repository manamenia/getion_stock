/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.dao;

import java.util.List;

/**
 *
 * @author Hamza
 */

public interface IGenericDao<E>{

    
    public E save(E entity);
    public E update(E entity);
    public List<E> selectAll();
    public List<E> selectAll(String x, String y);
    public E getById(int id);
    public void remove(int id);
    public E findOne(String paramNum, Object paramValue);
    public E findOne(String[] paramNum, Object[] paramValue);
    public int findCountBy(String paramNum, String paramValue);
 
    
}
