/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service;

import com.stock.entity.Commandefournisseur;
import java.util.List;


/**
 *
 * @author Hamza
 */
public interface ICommandeFournisService{
    
    public Commandefournisseur save(Commandefournisseur entity);
    public Commandefournisseur update(Commandefournisseur entity);
    public List<Commandefournisseur> selectAll();
    public List<Commandefournisseur> selectAll(String x, String y);
    public Commandefournisseur getById(int id);
    public void remove(int id);
    public Commandefournisseur findOne(String paramNum, Object paramValue);
    public Commandefournisseur findOne(String[] paramNum, Object[] paramValue);
    public int findCountBy(String paramNum, String paramValue);
    
}
