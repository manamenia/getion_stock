/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service;


import com.stock.entity.Lignevente;
import java.util.List;

/**
 *
 * @author Hamza
 */
public interface ILigneVenteService{
    
    public Lignevente save(Lignevente entity);
    public Lignevente update(Lignevente entity);
    public List<Lignevente> selectAll();
    public List<Lignevente> selectAll(String x, String y);
    public Lignevente getById(int id);
    public void remove(int id);
    public Lignevente findOne(String paramNum, Object paramValue);
    public Lignevente findOne(String[] paramNum, Object[] paramValue);
    public int findCountBy(String paramNum, String paramValue);
    
    
}
