/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service;

import com.stock.dao.*;
import com.stock.entity.Fournisseur;
import com.stock.entity.Lignecmdclient;
import com.stock.entity.Lignecmdfournis;
import java.util.List;

/**
 *
 * @author Hamza
 */
public interface ILigneCmdeFourniService{
    
    
    public Lignecmdfournis save(Lignecmdfournis entity);
    public Lignecmdfournis update(Lignecmdfournis entity);
    public List<Lignecmdfournis> selectAll();
    public List<Lignecmdfournis> selectAll(String x, String y);
    public Lignecmdfournis getById(int id);
    public void remove(int id);
    public Lignecmdfournis findOne(String paramNum, Object paramValue);
    public Lignecmdfournis findOne(String[] paramNum, Object[] paramValue);
    public int findCountBy(String paramNum, String paramValue);
    
    
}
