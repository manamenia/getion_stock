/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service;

import com.stock.entity.Article;
import com.stock.entity.Mvtstock;
import java.util.List;


/**
 *
 * @author Hamza
 */
public interface IMvtStockService{
    
    public Mvtstock save(Mvtstock entity);
    public Mvtstock update(Mvtstock entity);
    public List<Mvtstock> selectAll();
    public List<Mvtstock> selectAll(String x, String y);
    public Mvtstock getById(int id);
    public void remove(int id);
    public Mvtstock findOne(String paramNum, Object paramValue);
    public Mvtstock findOne(String[] paramNum, Object[] paramValue);
    public int findCountBy(String paramNum, String paramValue);
    
}
