/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service;

import com.stock.dao.*;
import com.stock.entity.Fournisseur;
import com.stock.entity.Lignecmdclient;
import java.util.List;

/**
 *
 * @author Hamza
 */
public interface ILigneCmdeClientService{
    
    
    public Lignecmdclient save(Lignecmdclient entity);
    public Lignecmdclient update(Lignecmdclient entity);
    public List<Lignecmdclient> selectAll();
    public List<Lignecmdclient> selectAll(String x, String y);
    public Lignecmdclient getById(int id);
    public void remove(int id);
    public Lignecmdclient findOne(String paramNum, Object paramValue);
    public Lignecmdclient findOne(String[] paramNum, Object[] paramValue);
    public int findCountBy(String paramNum, String paramValue);
    
    
}
