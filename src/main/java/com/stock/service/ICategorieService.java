/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service;

import com.stock.entity.Article;
import com.stock.entity.Category;
import java.util.List;


/**
 *
 * @author Hamza
 */
public interface ICategorieService{
    
   public Category save(Category Category);
    public Category update(Category entity);
    public List<Category> selectAll();
    public List<Category> selectAll(String x, String y);
    public Category getById(int id);
    public void remove(int id);
    public Category findOne(String paramNum, Object paramValue);
    public Category findOne(String[] paramNum, Object[] paramValue);
    public int findCountBy(String paramNum, String paramValue);
    
}
