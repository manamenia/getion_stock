/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service;

import com.stock.entity.Article;
import java.util.List;


/**
 *
 * @author Hamza
 */
public interface IArticleService{
    
    public Article save(Article entity);
    public Article update(Article entity);
    public List<Article> selectAll();
    public List<Article> selectAll(String x, String y);
    public Article getById(int id);
    public void remove(int id);
    public Article findOne(String paramNum, Object paramValue);
    public Article findOne(String[] paramNum, Object[] paramValue);
    public int findCountBy(String paramNum, String paramValue);
    
}
