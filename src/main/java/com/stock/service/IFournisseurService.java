/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service;

import com.stock.dao.*;
import com.stock.entity.Fournisseur;
import java.util.List;

/**
 *
 * @author Hamza
 */
public interface IFournisseurService{
    
    
    public Fournisseur save(Fournisseur entity);
    public Fournisseur update(Fournisseur entity);
    public List<Fournisseur> selectAll();
    public List<Fournisseur> selectAll(String x, String y);
    public Fournisseur getById(int id);
    public void remove(int id);
    public Fournisseur findOne(String paramNum, Object paramValue);
    public Fournisseur findOne(String[] paramNum, Object[] paramValue);
    public int findCountBy(String paramNum, String paramValue);
    
    
}
