/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service.Impl;

import com.stock.dao.ICommandeClientDao;
import com.stock.dao.ICommandeFournisseurDao;
import com.stock.entity.Commandeclient;
import com.stock.entity.Commandefournisseur;
import com.stock.service.ICommandeClientService;
import com.stock.service.ICommandeFournisService;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hamza
 */
@Transactional
public class CommandeFournisServiceImpl implements ICommandeFournisService{
    
    
    private ICommandeFournisseurDao dao;

   @Override
    public Commandefournisseur save(Commandefournisseur entity) { 
        return dao.save(entity);
    }

    @Override
    public Commandefournisseur update(Commandefournisseur entity) {
        return dao.update(entity);
    }

    @Override
    public List<Commandefournisseur> selectAll() {
        return dao.selectAll();
    }

    @Override
    public List<Commandefournisseur> selectAll(String x, String y) {
        return dao.selectAll(x, y);
    }

    @Override
    public Commandefournisseur getById(int id) {
        return dao.getById(id);
    }

    @Override
    public void remove(int id) {
       dao.remove(id);
    }

    @Override
    public Commandefournisseur findOne(String paramNum, Object paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public Commandefournisseur findOne(String[] paramNum, Object[] paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public int findCountBy(String paramNum, String paramValue) {

        return dao.findCountBy(paramNum, paramValue);
    }
    
}
