/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service.Impl;

import com.stock.dao.ICommandeClientDao;
import com.stock.entity.Commandeclient;
import com.stock.service.ICommandeClientService;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hamza
 */
@Transactional
public class CommandeClientServiceImpl implements ICommandeClientService{
    
    
    private ICommandeClientDao dao;

   @Override
    public Commandeclient save(Commandeclient entity) { 
        return dao.save(entity);
    }

    @Override
    public Commandeclient update(Commandeclient entity) {
        return dao.update(entity);
    }

    @Override
    public List<Commandeclient> selectAll() {
        return dao.selectAll();
    }

    @Override
    public List<Commandeclient> selectAll(String x, String y) {
        return dao.selectAll(x, y);
    }

    @Override
    public Commandeclient getById(int id) {
        return dao.getById(id);
    }

    @Override
    public void remove(int id) {
       dao.remove(id);
    }

    @Override
    public Commandeclient findOne(String paramNum, Object paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public Commandeclient findOne(String[] paramNum, Object[] paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public int findCountBy(String paramNum, String paramValue) {

        return dao.findCountBy(paramNum, paramValue);
    }
    
}
