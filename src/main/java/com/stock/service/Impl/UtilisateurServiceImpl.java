/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service.Impl;

import com.stock.dao.IArticleDao;
import com.stock.dao.IUtilisateurDao;
import com.stock.entity.Article;
import com.stock.entity.Utilisateur;
import com.stock.service.IArticleService;
import com.stock.service.IUtilisateurService;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hamza
 */
@Transactional
public class UtilisateurServiceImpl implements IUtilisateurService{
    
    
    private IUtilisateurDao dao;
    
   @Override
    public Utilisateur save(Utilisateur entity) { 
        return dao.save(entity);
    }

    @Override
    public Utilisateur update(Utilisateur entity) {
        return dao.update(entity);
    }

    @Override
    public List<Utilisateur> selectAll() {
        return dao.selectAll();
    }

    @Override
    public List<Utilisateur> selectAll(String x, String y) {
        return dao.selectAll(x, y);
    }

    @Override
    public Utilisateur getById(int id) {
        return dao.getById(id);
    }

    @Override
    public void remove(int id) {
       dao.remove(id);
    }

    @Override
    public Utilisateur findOne(String paramNum, Object paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public Utilisateur findOne(String[] paramNum, Object[] paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public int findCountBy(String paramNum, String paramValue) {

        return dao.findCountBy(paramNum, paramValue);
    }
    
}
