/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service.Impl;


import com.stock.dao.IVenteDao;
import com.stock.entity.Vente;
import com.stock.service.IVenteService;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hamza
 */
@Transactional
public class VenteServiceImpl implements IVenteService{   
    
    private IVenteDao dao;

   @Override
    public Vente save(Vente entity) { 
        return dao.save(entity);
    }

    @Override
    public Vente update(Vente entity) {
        return dao.update(entity);
    }

    @Override
    public List<Vente> selectAll() {
        return dao.selectAll();
    }

    @Override
    public List<Vente> selectAll(String x, String y) {
        return dao.selectAll(x, y);
    }

    @Override
    public Vente getById(int id) {
        return dao.getById(id);
    }

    @Override
    public void remove(int id) {
       dao.remove(id);
    }

    @Override
    public Vente findOne(String paramNum, Object paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public Vente findOne(String[] paramNum, Object[] paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public int findCountBy(String paramNum, String paramValue) {

        return dao.findCountBy(paramNum, paramValue);
    }
    
}
