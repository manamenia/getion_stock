/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service.Impl;

import com.stock.dao.IArticleDao;
import com.stock.dao.IMvtStockDao;
import com.stock.entity.Article;
import com.stock.entity.Mvtstock;
import com.stock.service.IArticleService;
import com.stock.service.IMvtStockService;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hamza
 */
@Transactional
public class MvtStockServiceImpl implements IMvtStockService{
    
    
    private IMvtStockDao dao;

    @Override
    public Mvtstock save(Mvtstock entity) { 
        return dao.save(entity);
    }

    @Override
    public Mvtstock update(Mvtstock entity) {
        return dao.update(entity);
    }

    @Override
    public List<Mvtstock> selectAll() {
        return dao.selectAll();
    }

    @Override
    public List<Mvtstock> selectAll(String x, String y) {
        return dao.selectAll(x, y);
    }

    @Override
    public Mvtstock getById(int id) {
        return dao.getById(id);
    }

    @Override
    public void remove(int id) {
       dao.remove(id);
    }

    @Override
    public Mvtstock findOne(String paramNum, Object paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public Mvtstock findOne(String[] paramNum, Object[] paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public int findCountBy(String paramNum, String paramValue) {

        return dao.findCountBy(paramNum, paramValue);
    }
    
    
}
