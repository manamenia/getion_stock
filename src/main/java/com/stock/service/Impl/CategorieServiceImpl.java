/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service.Impl;

import com.stock.dao.IArticleDao;
import com.stock.dao.ICategorieDao;
import com.stock.entity.Category;

import com.stock.service.ICategorieService;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hamza
 */
@Transactional
public class CategorieServiceImpl implements ICategorieService{
    
    
    private ICategorieDao dao;

    @Override
    public Category save(Category entity) { 
        return dao.save(entity);
    }

    @Override
    public Category update(Category entity) {
        return dao.update(entity);
    }

    @Override
    public List<Category> selectAll() {
        return dao.selectAll();
    }

    @Override
    public List<Category> selectAll(String x, String y) {
        return dao.selectAll(x, y);
    }

    @Override
    public Category getById(int id) {
        return dao.getById(id);
    }

    @Override
    public void remove(int id) {
       dao.remove(id);
    }

    @Override
    public Category findOne(String paramNum, Object paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public Category findOne(String[] paramNum, Object[] paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public int findCountBy(String paramNum, String paramValue) {

        return dao.findCountBy(paramNum, paramValue);
    }
    
}
