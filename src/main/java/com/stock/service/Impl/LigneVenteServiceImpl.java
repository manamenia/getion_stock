/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service.Impl;

import com.stock.dao.IArticleDao;
import com.stock.dao.ILigneVenteDao;
import com.stock.entity.Article;
import com.stock.entity.Lignevente;
import com.stock.service.IArticleService;
import com.stock.service.ILigneVenteService;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hamza
 */
@Transactional
public class LigneVenteServiceImpl implements ILigneVenteService{
    
    private ILigneVenteDao dao;

    @Override
    public Lignevente save(Lignevente entity) { 
        return dao.save(entity);
    }

    @Override
    public Lignevente update(Lignevente entity) {
        return dao.update(entity);
    }

    @Override
    public List<Lignevente> selectAll() {
        return dao.selectAll();
    }

    @Override
    public List<Lignevente> selectAll(String x, String y) {
        return dao.selectAll(x, y);
    }

    @Override
    public Lignevente getById(int id) {
        return dao.getById(id);
    }

    @Override
    public void remove(int id) {
       dao.remove(id);
    }

    @Override
    public Lignevente findOne(String paramNum, Object paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public Lignevente findOne(String[] paramNum, Object[] paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public int findCountBy(String paramNum, String paramValue) {

        return dao.findCountBy(paramNum, paramValue);
    }
    
}
