/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service.Impl;

import com.stock.dao.ILigneCmdClientDao;
import com.stock.entity.Article;
import com.stock.entity.Lignecmdclient;
import com.stock.service.IArticleService;
import com.stock.service.ILigneCmdeClientService;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hamza
 */
@Transactional
public class LigneCmdeClientServiceImpl implements ILigneCmdeClientService{
    
    
    private ILigneCmdClientDao dao;
    
    @Override
    public Lignecmdclient save(Lignecmdclient entity) { 
        return dao.save(entity);
    }

    @Override
    public Lignecmdclient update(Lignecmdclient entity) {
        return dao.update(entity);
    }

    @Override
    public List<Lignecmdclient> selectAll() {
        return dao.selectAll();
    }

    @Override
    public List<Lignecmdclient> selectAll(String x, String y) {
        return dao.selectAll(x, y);
    }

    @Override
    public Lignecmdclient getById(int id) {
        return dao.getById(id);
    }

    @Override
    public void remove(int id) {
       dao.remove(id);
    }

    @Override
    public Lignecmdclient findOne(String paramNum, Object paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public Lignecmdclient findOne(String[] paramNum, Object[] paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public int findCountBy(String paramNum, String paramValue) {

        return dao.findCountBy(paramNum, paramValue);
    }
    

    
}
