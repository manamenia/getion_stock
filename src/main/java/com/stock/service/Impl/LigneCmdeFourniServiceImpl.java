/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service.Impl;

import com.stock.dao.ILigneCmdClientDao;
import com.stock.dao.ILigneCmdFournisDao;
import com.stock.entity.Article;
import com.stock.entity.Lignecmdclient;
import com.stock.entity.Lignecmdfournis;
import com.stock.service.IArticleService;
import com.stock.service.ILigneCmdeClientService;
import com.stock.service.ILigneCmdeFourniService;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hamza
 */
@Transactional
public class LigneCmdeFourniServiceImpl implements ILigneCmdeFourniService{
    
    
    private ILigneCmdFournisDao dao;
    
    @Override
    public Lignecmdfournis save(Lignecmdfournis entity) { 
        return dao.save(entity);
    }

    @Override
    public Lignecmdfournis update(Lignecmdfournis entity) {
        return dao.update(entity);
    }

    @Override
    public List<Lignecmdfournis> selectAll() {
        return dao.selectAll();
    }

    @Override
    public List<Lignecmdfournis> selectAll(String x, String y) {
        return dao.selectAll(x, y);
    }

    @Override
    public Lignecmdfournis getById(int id) {
        return dao.getById(id);
    }

    @Override
    public void remove(int id) {
       dao.remove(id);
    }

    @Override
    public Lignecmdfournis findOne(String paramNum, Object paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public Lignecmdfournis findOne(String[] paramNum, Object[] paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public int findCountBy(String paramNum, String paramValue) {

        return dao.findCountBy(paramNum, paramValue);
    }
    
    
}
