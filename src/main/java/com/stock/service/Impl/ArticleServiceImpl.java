/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service.Impl;

import com.stock.dao.IArticleDao;
import com.stock.entity.Article;
import com.stock.service.IArticleService;
import java.util.List;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hamza
 */
@Transactional
public class ArticleServiceImpl implements IArticleService{
    
    
    private IArticleDao dao;

    @Override
    public Article save(Article entity) { 
        return dao.save(entity);
    }

    @Override
    public Article update(Article entity) {
        return dao.update(entity);
    }

    @Override
    public List<Article> selectAll() {
        return dao.selectAll();
    }

    @Override
    public List<Article> selectAll(String x, String y) {
        return dao.selectAll(x, y);
    }

    @Override
    public Article getById(int id) {
        return dao.getById(id);
    }

    @Override
    public void remove(int id) {
       dao.remove(id);
    }

    @Override
    public Article findOne(String paramNum, Object paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public Article findOne(String[] paramNum, Object[] paramValue) {
        return dao.findOne(paramNum, paramValue);
    }

    @Override
    public int findCountBy(String paramNum, String paramValue) {

        return dao.findCountBy(paramNum, paramValue);
    }
    
}
