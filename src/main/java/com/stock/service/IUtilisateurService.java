/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service;

import com.stock.entity.Article;
import com.stock.entity.Utilisateur;
import java.util.List;


/**
 *
 * @author Hamza
 */
public interface IUtilisateurService{
    
    public Utilisateur save(Utilisateur entity);
    public Utilisateur update(Utilisateur entity);
    public List<Utilisateur> selectAll();
    public List<Utilisateur> selectAll(String x, String y);
    public Utilisateur getById(int id);
    public void remove(int id);
    public Utilisateur findOne(String paramNum, Object paramValue);
    public Utilisateur findOne(String[] paramNum, Object[] paramValue);
    public int findCountBy(String paramNum, String paramValue);
    
}
