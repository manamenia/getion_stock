/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service;

import com.stock.entity.Commandeclient;
import java.util.List;


/**
 *
 * @author Hamza
 */
public interface ICommandeClientService{
    
   public Commandeclient save(Commandeclient entity);
    public Commandeclient update(Commandeclient entity);
    public List<Commandeclient> selectAll();
    public List<Commandeclient> selectAll(String x, String y);
    public Commandeclient getById(int id);
    public void remove(int id);
    public Commandeclient findOne(String paramNum, Object paramValue);
    public Commandeclient findOne(String[] paramNum, Object[] paramValue);
    public int findCountBy(String paramNum, String paramValue);
    
}
