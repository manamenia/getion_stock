/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service;

import com.stock.entity.Article;
import com.stock.entity.Vente;
import java.util.List;


/**
 *
 * @author Hamza
 */
public interface IVenteService{
    
    public Vente save(Vente entity);
    public Vente update(Vente entity);
    public List<Vente> selectAll();
    public List<Vente> selectAll(String x, String y);
    public Vente getById(int id);
    public void remove(int id);
    public Vente findOne(String paramNum, Object paramValue);
    public Vente findOne(String[] paramNum, Object[] paramValue);
    public int findCountBy(String paramNum, String paramValue);
    
}
