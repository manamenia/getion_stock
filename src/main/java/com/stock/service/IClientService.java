/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stock.service;

import com.stock.entity.Client;
import java.util.List;


/**
 *
 * @author Hamza
 */
public interface IClientService{
    
    public Client save(Client entity);
    public Client update(Client entity);
    public List<Client> selectAll();
    public List<Client> selectAll(String x, String y);
    public Client getById(int id);
    public void remove(int id);
    public Client findOne(String paramNum, Object paramValue);
    public Client findOne(String[] paramNum, Object[] paramValue);
    public int findCountBy(String paramNum, String paramValue);
    
}
